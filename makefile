CXX = g++
CXXFLAGS = -Wall -pedantic -W -std=c++11 -fPIC
TARGET = layer-editor

UIC = uic
MOC = moc

INCLUDE_PATH = -I include/Qt/5.9.1/gcc_64 \
	-I include/Qt/5.9.1/gcc_64/QtCore \
	-I include/Qt/5.9.1/gcc_64/QtWidgets

LIBS_PATH = -L lib/Qt/5.9.1/gcc_64
LIBS = -lQt5Widgets -lQt5Gui -lQt5Core \
	-Wl,-rpath=lib/Qt/5.9.1/gcc_64

SRCS = main app mainwindow
MOC_SRCS = app mainwindow
UI_SRCS = mainwindow

OBJS = $(addsuffix .o, $(addprefix src/, $(SRCS)))
MOCS = $(addsuffix _moc.cpp, $(addprefix src/, $(MOC_SRCS)))
MOC_OBJS = $(addsuffix _moc.o, $(addprefix src/, $(MOC_SRCS)))
UI_FORMS = $(addsuffix _ui.ih, $(addprefix src/, $(UI_SRCS)))

.PONY: all clean run

all: $(UI_FORMS) $(MOCS) $(MOC_OBJS) $(OBJS)
	$(CXX) $(OBJS) $(MOC_OBJS) -o $(TARGET) $(LIBS_PATH) $(LIBS)

clean:
	rm -f src/*_ui.ih
	rm -f src/*.o
	rm -f $(TARGET)
	rm -f src/*_moc.cpp

run: clean all
	./$(TARGET)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDE_PATH) -c $< -o $@

%_moc.cpp: %.h
	$(MOC) $< -o $@

%_ui.ih: %.ui
	$(UIC) $< -o $@
