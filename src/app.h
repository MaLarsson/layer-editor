/**/
#pragma once

#include <QApplication>

class CLayerEditApp : public QApplication
{
	Q_OBJECT

 public:
	CLayerEditApp(int& argc, char* argv[]);
	~CLayerEditApp() = default;

 private:
	
};
