/**/
#pragma once

#include "mainwindow_ui.ih"
#include <QMainWindow>

class CMainWindow : public QMainWindow
{
    Q_OBJECT

 public:
    CMainWindow(QWidget* parent=nullptr, Qt::WindowFlags f=0);
    ~CMainWindow() = default;

 private slots:
	void import();

 private:
	QScopedPointer<Ui::MainWindow> ui_;
};
