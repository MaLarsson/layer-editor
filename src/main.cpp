/**/
#include "app.h"
#include "mainwindow.h"

int main(int argc, char* argv[])
{
	CLayerEditApp app(argc, argv);

	CMainWindow mainWin;
	mainWin.show();

	return app.exec();
}
