/**/
#include "mainwindow.h"

#include <QPushButton>
#include <QOpenGLWidget>
#include <QSplitter>
#include <QFileDialog>
#include <QString>

CMainWindow::CMainWindow(QWidget* parent, Qt::WindowFlags f)
	: QMainWindow(parent, f), ui_(new Ui::MainWindow)
{
	ui_->setupUi(this);

	QSplitter* splitter = new QSplitter(this);
	QPushButton* button = new QPushButton("Push Me!");
	QOpenGLWidget* openGL = new QOpenGLWidget;

	ui_->layout->addWidget(splitter);
	splitter->addWidget(button);
	splitter->addWidget(openGL);

	connect(ui_->actionImport, SIGNAL(triggered()),
			this, SLOT(import()));
}


void CMainWindow::import()
{
	QString file = QFileDialog::getOpenFileName(this);

	// TODO... make use of the file path...
	qDebug("%s", qUtf8Printable(file));
}
